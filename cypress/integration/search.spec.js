describe('Test company list', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000');
    });

    it('verifies it can search using the dashboard', () => {
        cy.contains('Kompany');

        cy.get('input').type('Reilly Group');

        cy.get('button[aria-label="Zoeken"]').click();

        cy.contains('Reilly Group');

        cy.contains('Wintheiser Group').should('not.exist');
    });

    it('verifies it can open a detail screen', () => {
        cy.contains('Kompany');

        cy.contains('Details').click();

        cy.contains('Gemaakt op');

        // Moment of appreciation
        cy.wait(3000);

        cy.contains('Sluiten').click();
    });
});
