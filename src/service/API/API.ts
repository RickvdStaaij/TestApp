import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { Company, CompanyDetails } from '../../types/company';

const client = (): AxiosInstance => {
    return axios.create({
        baseURL: 'https://617c09aad842cf001711c200.mockapi.io/v1',
        timeout: 10000,
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
    });
};

export const getCompanies = async (search: string | null): Promise<Company[]> => {
    const uri = `companies${search ? `?search=${encodeURIComponent(search)}` : ''}`;

    const response: AxiosResponse = await client().get(uri);

    return response.data.data;
};

export const getCompanyDetails = async (id: string): Promise<CompanyDetails> => {
    const response: AxiosResponse = await client().get(`companies/${id}/details`);

    return response.data.data[0];
};
