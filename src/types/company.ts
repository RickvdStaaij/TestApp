export type Company = {
    id: string; // not my idea
    name: string;
    city: string;
    zipCode: string;
    streetName: string;
    logo: string;
    createdAt: string;
};

export type CompanyDetails = {
    [name: string]: string;
};
