import styled from 'styled-components';

export const Form = styled.form`
    display: flex;
    width: 100%;
    height: 3rem;
    margin-bottom: 2rem;
`;

export const Input = styled.input`
    flex-grow: 1;
    margin-right: 2rem;
    border: 0.0625rem solid #757575;
    border-radius: 0.3rem;
    padding: 0 1rem;
`;

export const Button = styled.button`
    padding: 0 1rem;
    border-radius: 0.3rem;
    background: #aa418c;
    border: 0;
    color: white;
    cursor: pointer;
`;
