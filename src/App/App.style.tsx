import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    * {
        padding: 0;
        margin: 0;
    }

    body, input, button {
        font-family: 'Quicksand', sans-serif;
        font-size: 18px;
    }
`;
