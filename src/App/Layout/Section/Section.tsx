import { ReactElement, ReactNode } from 'react';
import styled from 'styled-components';

type Props = {
    children: ReactNode;
};

const SectionSpace = styled.div`
    margin: 3rem 0;
`;

const Section = ({ children }: Props): ReactElement => {
    return <SectionSpace>{children}</SectionSpace>;
};

export default Section;
