import { ReactElement } from 'react';
import Container from '../Container';
import { Bar, Logo } from './Header.style';

const Header = (): ReactElement => (
    <Bar>
        <Container>
            <Logo>Kompany</Logo>
        </Container>
    </Bar>
);

export default Header;
