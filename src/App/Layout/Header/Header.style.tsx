import styled from 'styled-components';

export const Bar = styled.header`
    position: relative;
    background: white;
    padding: 1.5rem 0 1rem;
    box-shadow: 0 3px 6px 0 rgb(52 52 52 / 6%), 0 7px 14px 0 hsl(0deg 0% 46% / 10%);

    &::before {
        position: absolute;
        top: 0;
        content: '';
        height: 0.5rem;
        background-image: linear-gradient(90deg, #aa418c, #ff9300);
        display: block;
        width: 100%;
    }
`;

export const Logo = styled.div`
    font-family: 'Passion One', cursive;
    font-size: 2rem;
    text-transform: uppercase;
    color: #00526e;
`;
