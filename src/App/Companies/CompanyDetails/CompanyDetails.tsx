import { ReactElement, useEffect, useState } from 'react';
import { Body, Close, Smoke, Title, TitleBar, Window, Detail, Key, Value } from './CompanyDetails.style';
import { Company, CompanyDetails } from '../../../types/company';
import { getCompanyDetails } from '../../../service/API';
import Alert from '../../../components/Alert';

type Props = {
    id: string;
    company?: Company;
    onClose: () => void;
};

const CompanyDetailView = ({ id, onClose, company }: Props): ReactElement => {
    const [details, setDetails] = useState<CompanyDetails | null>(null);
    const [alert, setAlert] = useState<ReactElement | null>(null);

    useEffect(() => {
        const loadDetails = async () => {
            try {
                const loadedDetails = await getCompanyDetails(id);

                setDetails(loadedDetails);
            } catch (error) {
                setAlert(<Alert>De data kon niet worden geladen, probeer het later nog eens.</Alert>);
            }
        };

        loadDetails();
    }, []);

    return (
        <Smoke>
            <Window>
                <TitleBar>
                    <Title>Details {company ? company.name : id}</Title>
                    <Close onClick={onClose}>Sluiten</Close>
                </TitleBar>
                <Body>
                    {company && (
                        <>
                            <Detail>
                                <Key>Naam</Key>
                                <Value>{company.name}</Value>
                            </Detail>
                            <Detail>
                                <Key>Adres</Key>
                                <Value>{company.streetName}</Value>
                            </Detail>
                            <Detail>
                                <Key>Postcode</Key>
                                <Value>{company.zipCode}</Value>
                            </Detail>
                            <Detail>
                                <Key>Stad</Key>
                                <Value>{company.city}</Value>
                            </Detail>
                            <Detail>
                                <Key>Gemaakt op</Key>
                                <Value>{company.createdAt}</Value>
                            </Detail>
                        </>
                    )}
                    {!details && <div>Loading company details...</div>}
                    {alert}
                    {details &&
                        Object.keys(details).map((key, index) => (
                            <Detail>
                                <Key>{key}</Key>
                                <Value>{details[key]}</Value>
                            </Detail>
                        ))}
                </Body>
            </Window>
        </Smoke>
    );
};

export default CompanyDetailView;
