import styled from 'styled-components';

export const Smoke = styled.div`
    position: fixed;
    display: flex;
    width: 100%;
    height: 100vh;
    top: 0;
    left: 0;
    background: rgba(0, 82, 110, 0.2);
    overflow: auto;
    justify-content: center;
    align-items: center;
`;

export const Window = styled.div`
    width: 50rem;
    max-width: calc(100% - 2rem);
`;

export const TitleBar = styled.div`
    display: flex;
    background: rgba(0, 82, 110);
    border-radius: 0.3rem 0.3rem 0 0;
`;

export const Title = styled.div`
    flex-grow: 1;
    color: white;
    padding: 1rem;
`;

export const Close = styled.button`
    color: white;
    padding: 1rem;
    margin: 0;
    border: 0;
    background: transparent;
    cursor: pointer;
`;

export const Body = styled.div`
    padding: 1rem 1rem 0.1rem;
    background: white;
    border-radius: 0 0 0.3rem 0.3rem;
`;

export const Detail = styled.div`
    display: flex;
    margin-bottom: 1rem;
`;

export const Key = styled.div`
    width: 30%;
`;

export const Value = styled.div`
    flex-grow: 1;
`;
