import React from 'react';
import { render, screen } from '@testing-library/react';
import Companies from './Companies';
import { Company } from '../../types/company';
import { getCompanies } from '../../service/API';

jest.mock('../../service/API', () => ({
    getCompanies: jest.fn(),
}));

const companyResult: Company[] = [
    {
        id: '1',
        name: 'Wintheiser Group',
        city: 'West Esteban',
        zipCode: '97018',
        streetName: 'Lilly View',
        logo: 'https://via.placeholder.com/150',
        createdAt: '2021-07-16T19:41:28.272Z',
    },
    {
        id: '2',
        name: 'Feest, Schinner and Lowe',
        city: 'New Ahmad',
        zipCode: '07811',
        streetName: 'Bartell Tunnel',
        logo: 'https://via.placeholder.com/150',
        createdAt: '2021-10-03T18:37:01.931Z',
    },
];

const mockedGetCompanies = jest.mocked(getCompanies, true);

test('Check if companies are listed', async () => {
    mockedGetCompanies.mockResolvedValue(companyResult);

    render(<Companies />);

    expect(await screen.findByText(/Wintheiser Group/i)).toBeVisible();
    expect(await screen.findByText(/Feest, Schinner and Lowe/i)).toBeVisible();
});

test('Check error message is shown on data failure', async () => {
    mockedGetCompanies.mockImplementation(() => Promise.reject(new Error('Failed.')));

    render(<Companies />);

    expect(await screen.findByText(/probeer het later nog eens/i)).toBeVisible();
});
