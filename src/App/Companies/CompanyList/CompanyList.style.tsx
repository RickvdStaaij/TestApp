import styled from 'styled-components';

export const Logo = styled.img`
    width: 3rem;
    margin-right: 1rem;
`;

export const Details = styled.div`
    color: #999999;
    flex-grow: 1;
`;

export const Name = styled.div`
    display: block;
    color: #000;
    transition: color 300ms;
`;

export const CompanyItem = styled.div`
    display: flex;
    border-bottom: 0.15rem solid #ddd;
    padding: 1rem 0.5rem;
    transition: border-bottom 300ms;

    &:hover {
        border-bottom: 0.15rem solid #aa418c;

        ${Name} {
            color: #aa418c;
        }
    }
`;

export const MoreDetails = styled.button`
    border: 1px solid #aa418c;
    cursor: pointer;
    color: #aa418c;
    background: white;
    border-radius: 0.3rem;
    padding: 0 0.5rem;
    transition: background 300ms, color 300ms;

    &:hover {
        background: #aa418c;
        color: white;
    }
`;
